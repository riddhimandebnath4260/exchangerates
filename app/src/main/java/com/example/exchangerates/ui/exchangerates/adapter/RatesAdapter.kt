package com.example.exchangerates.ui.exchangerates.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.exchangerates.databinding.ItemRatesBinding
import com.example.exchangerates.model.RatesFullName

class RatesAdapter(private val fullNameItems:List<RatesFullName>) : Adapter<ViewHolder>() {

    private val mDiffer = AsyncListDiffer(this, RatesDiffUtil())



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemRatesBinding = ItemRatesBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return RatesViewHolder(itemRatesBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mDiffer.currentList[position]
        val item1 = fullNameItems[position].ratesFullname
        (holder as RatesViewHolder).bind(item,item1)
    }

    override fun getItemCount(): Int {
        return mDiffer.currentList.size
    }

    fun submitList(list: List<Pair<String, Double>>) {
       mDiffer.submitList(list)
    }

    inner class RatesViewHolder(
        private val itemRatesBinding: ItemRatesBinding
    ) : ViewHolder(itemRatesBinding.root) {


        fun bind(rates: Pair<String, Double>, item1: String) {
            itemRatesBinding.exchangeRatesKey.text = rates.first
            itemRatesBinding.exchangeRatesValue.text = rates.second.toString()
            itemRatesBinding.exchangeRateFullName.text = item1
        }
    }


}