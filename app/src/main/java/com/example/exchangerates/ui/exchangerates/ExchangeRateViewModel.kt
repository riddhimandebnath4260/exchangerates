package com.example.exchangerates.ui.exchangerates

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exchangerates.BuildConfig
import com.example.exchangerates.data.repos.rates.RatesRepository
import com.example.exchangerates.data.source.local.RatesFullNameDatabase
import com.example.exchangerates.helper.Results
import com.example.exchangerates.model.RatesFullName
import kotlinx.coroutines.launch

class ExchangeRateViewModel(private val repository: RatesRepository,
private val ratesFullNameDatabase: RatesFullNameDatabase):ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _rates = MutableLiveData<Map<String,Double>>()
    val rates: LiveData<Map<String,Double>> = _rates

    private val _ratesFullName = MutableLiveData<List<RatesFullName>>()
    val ratesFullName: LiveData<List<RatesFullName>> = _ratesFullName

    private val _errorResult = MutableLiveData<String>()
    val errorResult: LiveData<String> = _errorResult

    fun loadRatesList(
        base: String?=null,
        symbols:String?=null,
        prettyPrint: Boolean?=null,
        showAlternative: Boolean?=null
    ){
        viewModelScope.launch{
            _loading.postValue(true)

            val result = repository.loadRates(
                appId = BuildConfig.API_TOKEN,
                base = base,
                symbols = symbols,
                prettyPrint = prettyPrint,
                showAlternative = showAlternative,
            )

            when(result){
                is Results.Failure->{
                    _loading.postValue(false)
                    _errorResult.postValue(result.throwable.toString())
                    Log.w(TAG, result.throwable)
                }

                is Results.Success -> {
                    _loading.postValue(false)
                    _rates.postValue(result.value.rates)
                }

            }
        }
    }


    companion object {
        private const val TAG = "RatesViewModel"
    }

    fun loadRatesFullName(prettyPrint: Boolean?, showAlternative: Boolean?, showInactive: Boolean?) {
        viewModelScope.launch {


            val result = repository.loadRatesFullName(
                prettyPrint = prettyPrint,
                showAlternative = showAlternative,
                showInactive = showInactive
            )
            _ratesFullName.postValue(result)


        }
    }
}

private fun <T> MutableLiveData<T>.postValue(result: Unit) {

}

