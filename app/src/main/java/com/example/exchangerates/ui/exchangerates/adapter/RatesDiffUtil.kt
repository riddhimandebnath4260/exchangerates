package com.example.exchangerates.ui.exchangerates.adapter

import androidx.recyclerview.widget.DiffUtil

class RatesDiffUtil : DiffUtil.ItemCallback<Pair<String,Double>>() {


    override fun areContentsTheSame(oldItem: Pair<String,Double>, newItem: Pair<String,Double>): Boolean {
        return oldItem.first == newItem.first
    }

    override fun areItemsTheSame(oldItem: Pair<String,Double>, newItem: Pair<String,Double>): Boolean {
        return oldItem == newItem
    }

}