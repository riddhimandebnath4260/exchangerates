package com.example.exchangerates.data.repos.rates

import com.example.exchangerates.data.source.local.RatesFullNameDatabase
import com.example.exchangerates.data.source.remote.RatesApiService
import com.example.exchangerates.helper.ResolveApiResponse

object RatesRepositoryFact {
    private lateinit var ratesRepository: RatesRepository

    fun getInstance(
        ratesApiService: RatesApiService,
        resolveApiResponse: ResolveApiResponse,
        ratesFullNameDatabase: RatesFullNameDatabase
    ): RatesRepository {
        if (!RatesRepositoryFact::ratesRepository.isInitialized) {
            ratesRepository = RatesRepositoryImpl(ratesApiService, resolveApiResponse,ratesFullNameDatabase)
        }

        return ratesRepository
    }
}