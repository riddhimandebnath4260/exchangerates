package com.example.exchangerates.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.exchangerates.model.RatesFullName

@Dao
interface ExchangeRateFullNameDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addFullName(fullName: List<RatesFullName>)
    @Query("SELECT * FROM Rates_Fullname")
    fun readFullName():List<RatesFullName>
}