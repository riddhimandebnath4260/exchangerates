package com.example.exchangerates.data.repos.rates

import android.util.Log
import com.example.exchangerates.data.source.local.RatesFullNameDatabase
import com.example.exchangerates.data.source.remote.RatesApiService
import com.example.exchangerates.helper.ResolveApiResponse
import com.example.exchangerates.helper.Results
import com.example.exchangerates.model.RatesFullName
import com.example.exchangerates.model.RatesList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RatesRepositoryImpl (
    private val ratesApiService: RatesApiService,
    private val resolveApiResponse: ResolveApiResponse,
    ratesFullNameDatabase: RatesFullNameDatabase
) : RatesRepository {

    private val ratesInfo = ratesFullNameDatabase.exchangeRateFullNameDao()
    override suspend fun loadRates(
        appId: String,
        base : String?,
        symbols : String?,
        prettyPrint : Boolean?,
        showAlternative : Boolean?
    ): Results<RatesList> = withContext(Dispatchers.IO) {

        return@withContext resolveApiResponse.resolve(this.javaClass.name) {
            ratesApiService.rates(
                appId,base,symbols,prettyPrint,showAlternative
            )

        }
    }

    override suspend fun loadRatesFullName(
        prettyPrint: Boolean?,
        showAlternative: Boolean?,
        showInactive: Boolean?
    ) = withContext(Dispatchers.IO){
        val result = ratesApiService.ratesFullName()
        if (result.body()!=null){
            ratesInfo.addFullName(result.body()!!.map { RatesFullName(it.key,it.value) })
            Log.d("check",result.body().toString())
        }
    }




}
