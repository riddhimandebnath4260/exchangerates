package com.example.exchangerates.data.repos.rates

import com.example.exchangerates.helper.Results
import com.example.exchangerates.model.RatesList

interface RatesRepository {

    suspend fun loadRates(
        appId : String,
        base : String?,
        symbols : String?,
        prettyPrint : Boolean?,
        showAlternative : Boolean?
    ) : Results<RatesList>

    suspend fun loadRatesFullName(prettyPrint: Boolean?,showAlternative: Boolean?,showInactive:Boolean?)


}