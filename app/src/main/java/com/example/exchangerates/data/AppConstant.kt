package com.example.exchangerates.data

import androidx.viewbinding.BuildConfig



object AppConstant {
    val isDebug = BuildConfig.DEBUG
}
