package com.example.exchangerates.data.source.remote

import com.example.exchangerates.model.RatesList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApiService {
    @GET("latest.json")
    suspend fun rates(
        @Query("app_id") appId : String,
        @Query("base") base : String?,
        @Query("symbols") symbols : String?,
        @Query("prettyprint") prettyPrint : Boolean?,
        @Query("show_alternative") showAlternative : Boolean?
        ) : RatesList

    @GET("currencies.json")
    suspend fun ratesFullName() : Response<Map<String,String>>

}